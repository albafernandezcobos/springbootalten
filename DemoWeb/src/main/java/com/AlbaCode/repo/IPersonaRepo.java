package com.AlbaCode.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.AlbaCode.model.Persona;

public interface IPersonaRepo extends JpaRepository<Persona, Integer> {

	
}
