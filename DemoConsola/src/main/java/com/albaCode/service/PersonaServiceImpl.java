package com.albaCode.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.albaCode.repository.IPersona;
import com.albaCode.repository.PersonaRepoImpl1;

@Service

public class PersonaServiceImpl implements IPersonaService {
	@Autowired
	@Qualifier("persona1")
	IPersona repo;
	@Override
	public void registrarHandler(String pNombre) {
		//repo = new PersonaRepoImpl();
		repo.registrar(pNombre);
	}
}
