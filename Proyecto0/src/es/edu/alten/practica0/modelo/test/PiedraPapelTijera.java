package es.edu.alten.practica0.modelo.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.edu.alten.practica0.modelo.Lagarto;
import es.edu.alten.practica0.modelo.Papel;
import es.edu.alten.practica0.modelo.Piedra;
import es.edu.alten.practica0.modelo.PiedraPapelTijeraFactory;
import es.edu.alten.practica0.modelo.Spock;
import es.edu.alten.practica0.modelo.Tijera;



class PiedraPapelTijera {
//lote de pruebas
	PiedraPapelTijeraFactory piedra;
	PiedraPapelTijeraFactory papel;
	PiedraPapelTijeraFactory tijera;
	PiedraPapelTijeraFactory lagarto;
	PiedraPapelTijeraFactory spock;
	
	@BeforeEach
	void setUp() throws Exception {
		//se ejecuta antes de cada metodo
		piedra = new Piedra();
		papel = new Papel();
		tijera = new Tijera();
		lagarto = new Lagarto();
		spock = new Spock();
	}
	
	@AfterEach
	void tearDown() throws Exception {
		piedra = null;
		papel = null;
		tijera = null;
		lagarto = null;
		spock = null;
	}
	
	@Test
	void testGetInstancePiedra() {
		assertEquals("piedra", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PIEDRA)
														.getNombre()
														.toLowerCase());
		
	}

	@Test
	void testGetInstancePapel() {
		assertEquals("papel", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.PAPEL)
														.getNombre()
														.toLowerCase());
		
	}
	
	@Test
	void testGetInstanceTijera() {
		assertEquals("tijera", PiedraPapelTijeraFactory.getInstance(PiedraPapelTijeraFactory.TIJERA)
														.getNombre()
														.toLowerCase());
		
	}
	
	//casos piedra
	@Test
	void testCompararPiedraPierdePapel() {
		assertEquals(-1, piedra.comparar(papel));
		assertEquals("piedra perdi� con papel", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPiedraGanaATijera() {
		assertEquals(1, piedra.comparar(tijera));
		assertEquals("piedra gano a tijera", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPiedraEmpataConPiedra() {
		assertEquals(0, piedra.comparar(piedra));
		assertEquals("piedra empata con piedra", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPiedraGanaALagarto() {
		assertEquals(1, piedra.comparar(lagarto));
		assertEquals("piedra gano a lagarto", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPiedraPierdeConSpock() {
		assertEquals(-1, piedra.comparar(spock));
		assertEquals("piedra perdi� con spock", piedra.getDescripcionResultado()
													  .toLowerCase());
	}
	
	
	
	//casos papel
	@Test
	void testCompararPapelGanaAPiedra() {
		assertEquals(1, papel.comparar(piedra));
		assertEquals("papel gano a piedra", papel.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPapelPierdeConTijera() {
		assertEquals(-1, papel.comparar(tijera));
		assertEquals("papel perdi� con tijera", papel.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPapelEmpataConPapel() {
		assertEquals(0, papel.comparar(papel));
		assertEquals("papel empata con papel", papel.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPapelGanaASpock() {
		assertEquals(1, papel.comparar(spock));
		assertEquals("papel gano a spock", papel.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararPapelPierdeConLagarto() {
		assertEquals(-1, papel.comparar(lagarto));
		assertEquals("papel perdi� con lagarto", papel.getDescripcionResultado()
													  .toLowerCase());
	}
	
	//casos tijera
	@Test
	void testCompararTijeraGanaAPapel() {
		assertEquals(1, tijera.comparar(papel));
		assertEquals("tijera gano a papel", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararTijeraPierdeConPiedra() {
		assertEquals(-1, tijera.comparar(piedra));
		assertEquals("tijera perdi� con piedra", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararTijeraEmpataConTijera() {
		assertEquals(0, tijera.comparar(tijera));
		assertEquals("tijera empata con tijera", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararTijeraGanaALagarto() {
		assertEquals(1, tijera.comparar(lagarto));
		assertEquals("tijera gano a lagarto", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararTijeraPierdeConSpock() {
		assertEquals(-1, tijera.comparar(spock));
		assertEquals("tijera perdi� con spock", tijera.getDescripcionResultado()
													  .toLowerCase());
	}
	
	//casos spock
	@Test
	void testCompararSpockGanaAPiedra() {
		assertEquals(1, spock.comparar(piedra));
		assertEquals("spock gano a piedra", spock.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararSpockGanaATijera() {
		assertEquals(1, spock.comparar(tijera));
		assertEquals("spock gano a tijera", spock.getDescripcionResultado()
													  .toLowerCase());
	}
	@Test
	void testCompararSpockPierdeConLagrto() {
		assertEquals(-1, spock.comparar(lagarto));
		assertEquals("spock perdi� con lagarto", spock.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararSpockPierdeConPapel() {
		assertEquals(-1, spock.comparar(papel));
		assertEquals("spock perdi� con papel", spock.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararSpockEmpataConSpock() {
		assertEquals(0, spock.comparar(spock));
		assertEquals("spock empata con spock", spock.getDescripcionResultado()
													  .toLowerCase());
	}
	
	//Casos Lagarto
	@Test
	void testCompararLagartoGanaASpock() {
		assertEquals(1, lagarto.comparar(spock));
		assertEquals("lagarto gano a spock", lagarto.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararLagartoGanaAPapel() {
		assertEquals(1, lagarto.comparar(papel));
		assertEquals("lagarto gano a papel", lagarto.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararLagartoPierdeConPiedra() {
		assertEquals(-1, lagarto.comparar(piedra));
		assertEquals("lagarto perdi� con piedra", lagarto.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararLagartoPierdeConTijera() {
		assertEquals(-1, lagarto.comparar(tijera));
		assertEquals("lagarto perdi� con tijera", lagarto.getDescripcionResultado()
													  .toLowerCase());
	}
	
	@Test
	void testCompararLagartoEmpataConLagarto() {
		assertEquals(0, lagarto.comparar(lagarto));
		assertEquals("lagarto empata con lagarto", lagarto.getDescripcionResultado()
													  .toLowerCase());
	}
	
}
