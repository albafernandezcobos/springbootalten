package es.edu.alten.practica0.modelo;

public class Tijera extends PiedraPapelTijeraFactory {
	public Tijera() {
		this("tijera",TIJERA);
	}
	
	public Tijera(String pNom, int pNum) {
		super(pNom, pNum);
	}

	
	@Override
	public boolean isMe(int pNum) {
		return pNum==TIJERA;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedraPapelTijeraFactory) {
		int resul=0;
		switch (pPiedraPapelTijeraFactory.getNumero()) {
		//gana a
		case PAPEL:
		case LAGARTO:
			resul=1;
			this.descripcionResultado = "Tijera gano a " + pPiedraPapelTijeraFactory.getNombre();
			break;
		//pierde con 
		case PIEDRA:
		case SPOCK:
			resul=-1;
			this.descripcionResultado = "Tijera perdi� con " + pPiedraPapelTijeraFactory.getNombre();	
			break;
			
		case TIJERA:
			resul=0;
			this.descripcionResultado = "Tijera empata con " + pPiedraPapelTijeraFactory.getNombre();
			break;
			
			default:
				break;
		}
		return resul;
	}

}
