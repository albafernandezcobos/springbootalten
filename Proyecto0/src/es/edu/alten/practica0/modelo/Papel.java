package es.edu.alten.practica0.modelo;

public class Papel extends PiedraPapelTijeraFactory {

	public Papel() {
		this("papel", PAPEL);
	}
	public Papel(String pNom, int pNum) {
		super(pNom, pNum );		
	}

	@Override
	public boolean isMe(int pNum) {
		return pNum==PAPEL;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedraPapelTijeraFactory) {
		int resul=0;
		switch (pPiedraPapelTijeraFactory.getNumero()) {
		//gana a
		case PIEDRA:
		case SPOCK:
			resul=1;
			this.descripcionResultado = "Papel gano a " + pPiedraPapelTijeraFactory.getNombre();
			break;
			//pierde con 
		case TIJERA:
		case LAGARTO:
			resul=-1;
			this.descripcionResultado = "Papel perdi� con " + pPiedraPapelTijeraFactory.getNombre();
			break;
			
		case PAPEL:
			resul=0;
			this.descripcionResultado = "Papel empata con " + pPiedraPapelTijeraFactory.getNombre();
			break;
			
			default:
				break;
		}
		return resul;
	}

}
