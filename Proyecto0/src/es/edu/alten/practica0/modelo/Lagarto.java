package es.edu.alten.practica0.modelo;

public class Lagarto extends PiedraPapelTijeraFactory {

	public Lagarto() {
		this("lagarto", LAGARTO);
	}
	public Lagarto (String pNom, int pNum) {
		super (pNom, pNum);
	}
	
	@Override
	public boolean isMe(int pNum) {
		return pNum == LAGARTO;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedraPapelTijeraFactory) {
		int resul=0;
		switch (pPiedraPapelTijeraFactory.getNumero()) {
		//gana a
		case PAPEL:
		case SPOCK:
			resul=1;
			this.descripcionResultado = "Lagarto gano a " + pPiedraPapelTijeraFactory.getNombre();
			break;
		//pierde con 	
		case PIEDRA:
		case TIJERA:
			resul=-1;
			this.descripcionResultado = "Lagarto perdi� con " + pPiedraPapelTijeraFactory.getNombre();
			break;
		case LAGARTO:
			resul=0;
			this.descripcionResultado = "Lagarto empata con " + pPiedraPapelTijeraFactory.getNombre();
			break;
			default:
				break;
		}
		return resul;
	}

}
