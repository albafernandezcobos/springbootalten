package es.edu.alten.practica0.modelo;

public class Piedra extends PiedraPapelTijeraFactory {
	
	public Piedra() {
		this("piedra", PIEDRA);
	}
	
	public Piedra(String pNom, int pNum) {
		super(pNom, pNum);		
	}
	
	@Override
	public boolean isMe(int pNum) {
		return pNum==PIEDRA;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedraPapelTijeraFactory) {
		int resul=0;
		switch (pPiedraPapelTijeraFactory.getNumero()) {
		//gana a
		case TIJERA:
		case LAGARTO:
			resul=1;
			this.descripcionResultado = "Piedra gano a " + pPiedraPapelTijeraFactory.getNombre();
			break;
		//pierde con 
		case PAPEL:
		case SPOCK:
			resul=-1;
			this.descripcionResultado = "Piedra perdi� con " + pPiedraPapelTijeraFactory.getNombre();
			break;
			
		case PIEDRA:
			resul=0;
			this.descripcionResultado = "Piedra empata con " + pPiedraPapelTijeraFactory.getNombre();
			break;
			
			default:
				break;
		}
		return resul;
		}
	}


