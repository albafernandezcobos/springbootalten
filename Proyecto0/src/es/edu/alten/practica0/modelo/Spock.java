package es.edu.alten.practica0.modelo;

public class Spock extends PiedraPapelTijeraFactory {

	public Spock() {
		this("spock", SPOCK);
	}
	public Spock (String pNom, int pNum) {
		super (pNom, pNum);
	}
	
	
	
	@Override
	public boolean isMe(int pNum) {
		return pNum == SPOCK;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPiedraPapelTijeraFactory) {
		int resul=0;
		switch (pPiedraPapelTijeraFactory.getNumero()) {
		//gana a
		case TIJERA:
		case PIEDRA:
			resul=1;
			this.descripcionResultado = "Spock gano a " + pPiedraPapelTijeraFactory.getNombre();
			break;
		//pierde con 	
		case PAPEL:
		case LAGARTO:
			resul=-1;
			this.descripcionResultado = "Spock perdi� con " + pPiedraPapelTijeraFactory.getNombre();
			break;
			
		case SPOCK:
			resul=0;
			this.descripcionResultado = "Spock empata con " + pPiedraPapelTijeraFactory.getNombre();
			break;
			
			default:
				break;
		}
		return resul;
	}

}
