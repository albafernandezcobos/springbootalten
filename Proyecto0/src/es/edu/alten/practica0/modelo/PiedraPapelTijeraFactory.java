package es.edu.alten.practica0.modelo;

import java.util.ArrayList;
import java.util.List;


public abstract class PiedraPapelTijeraFactory {
	public final static int  PIEDRA = 1;
	public final static int  PAPEL = 2;
	public final static int TIJERA = 3;
	public final static int LAGARTO = 4;
	public final static int SPOCK = 5;
	//atributos
protected String descripcionResultado;
private static List<PiedraPapelTijeraFactory>elementos;
protected String nombre;
protected int numero;

//constructor
public PiedraPapelTijeraFactory(String pNom, int pNum){
	nombre=pNom;
	numero=pNum;
}
//getter y setter

public String getNombre() {
	return nombre;
}

public void setNombre(String nombre) {
	this.nombre = nombre;
}

public int getNumero() {
	return numero;
}

public void setNumero(int numero) {
	this.numero = numero;
}

public String getDescripcionResultado() {
	return descripcionResultado;
}
//metodo de negocio
public abstract boolean isMe(int pNum);
public abstract int comparar(PiedraPapelTijeraFactory pPiedraPapelTijeraFactory);

public static PiedraPapelTijeraFactory getInstance(int pNumero) {
	//el corazon del factory
	//1� el padre reconoce a todos sus hijos 
	elementos = new ArrayList<PiedraPapelTijeraFactory>();
	elementos.add(new Piedra());
	elementos.add(new Papel());
	elementos.add(new Tijera());
	//tengo que agregar 
	elementos.add(new Lagarto());
	elementos.add(new Spock());
	//este codigo va a ser siempre el mismo 
	for (PiedraPapelTijeraFactory piedraPapelTijeraFactory : elementos) {
		if (piedraPapelTijeraFactory.isMe(pNumero))
		return piedraPapelTijeraFactory;
	}
	return null;
}
}

