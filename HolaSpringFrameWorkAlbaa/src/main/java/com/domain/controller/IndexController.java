package com.domain.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import es.edu.alten.practica0.modelo.PiedraPapelTieraFactory;


@Controller
public class IndexController {
	@RequestMapping("/home")
	public String goIndex() {
		return "Index";
	}
	
	@RequestMapping("/")
	public String getPresentacion() {
		return "Prersentacion";
	}
	
	@RequestMapping("/juego/{nombre}")
	public String goPiedraPapelTijera(@PathVariable("nombre") String nombre, Model model) {		
		List<PiedraPapelTieraFactory> opciones = new ArrayList<PiedraPapelTieraFactory>();
		System.out.println("nombre=" + nombre);

		for(int i =1;i<6;i++)
			opciones.add(PiedraPapelTieraFactory.getInstance(i));
			
		model.addAttribute("nombre", nombre);
		model.addAttribute("opciones", opciones);		
		
		return "PiedraPapelTijera";
		}
	@RequestMapping("/resolverJuego")
	public String goResolverJuego(@RequestParam(required = false) Integer selOpcion, Model model) {
		
		System.out.println("******************** paso por /resolverJuego ********************:" + selOpcion );
		//seleccion de la computadora
		PiedraPapelTieraFactory computadora = PiedraPapelTieraFactory.getInstance((int)(Math.random()*100%5+1));
		//este es el parametro que viene desde el JSP
		PiedraPapelTieraFactory jugador = PiedraPapelTieraFactory.getInstance(selOpcion.intValue());
		
		jugador.comparar(computadora);
		
		model.addAttribute("jugador", jugador);
		model.addAttribute("computadora", computadora);
		model.addAttribute("resultado", jugador.getDescripcionREsultado());
		
		
		
		
		return "MostrarResultado";
	}
	@RequestMapping("/listado")
	public String goListado(Model model) {
//		List<com.domain.modelo.Model> alumnos=null ;
//		DAO aluDao = new AlumnoDAO();
//		try {
//			alumnos= aluDao.leer(null);
//		} catch (ClassNotFoundException | SQLException e) {			
//			e.printStackTrace();
//		}
		
		model.addAttribute("titulo", "Listado de alumnos");
		model.addAttribute("profesor", "Gabriel Casas");
//		model.addAttribute("alumnos", alumnos);
		
		return "Listado";
	}
}
