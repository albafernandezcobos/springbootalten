package com.domain.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.domain.modelo.Alumno;
import com.domain.modelo.Model;
import com.domain.util.ConnectionManager;

public class AlumnoDao implements DAO {

	public AlumnoDao() {
		
	}

	@Override
	public void agregar(Model pModel) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection con = ConnectionManager.getConnection();
		
		String sql = new String("insert into alumnos(alu_nombre, alu_apellido, alu_conocimientos,alu_git) values(?,?,?,? )");
		
		Alumno alu =(Alumno) pModel;
		PreparedStatement stm = con.prepareStatement(sql);
		stm.setString(1, alu.getNombre());
		stm.setString(2, alu.getApellido());
		stm.setString(3, alu.getEstudios());
		stm.setString(4, alu.getLinkArepositorio());		
		stm.execute();

	}

	@Override
	public void modificar(Model pModel) throws ClassNotFoundException, SQLException {
		

	}

	@Override
	public void eliminar(Model pModel) throws ClassNotFoundException, SQLException {
		

	}

	@Override
	public List<Model> leer(Model pModel) throws ClassNotFoundException, SQLException {
		
		return null;
	}

}
